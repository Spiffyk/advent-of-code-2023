const std = @import("std");

const NumWord = struct {
    w: []const u8,
    n: u8,
};

const numwords = [_]NumWord{
    .{ .w = "one", .n = 1 },
    .{ .w = "two", .n = 2 },
    .{ .w = "three", .n = 3 },
    .{ .w = "four", .n = 4 },
    .{ .w = "five", .n = 5 },
    .{ .w = "six", .n = 6 },
    .{ .w = "seven", .n = 7 },
    .{ .w = "eight", .n = 8 },
    .{ .w = "nine", .n = 9 },
};

pub fn main() !void
{
    std.debug.print("give input:\n", .{});

    var out = std.io.getStdOut();
    var bw = std.io.bufferedWriter(out.writer());
    var w = bw.writer();

    var sum: u64 = 0;
    var in = std.io.getStdIn();
    var br = std.io.bufferedReader(in.reader());
    var rd = br.reader();
    var buf: [1024]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buf);
    var fbw = fbs.writer();
    while (true) {
        fbs.reset();
        rd.streamUntilDelimiter(fbw, '\n', buf.len) catch |err| {
            if (err == error.EndOfStream) {
                break;
            } else {
                return err;
            }
        };

        const line = fbs.getWritten();
        if (line.len == 0)
            break;

        var first: ?u8 = null;
        var last: u8 = 0;
        for (line, 0..) |c, ix| {
            if (c >= '0' and c <= '9') {
                if (first == null)
                    first = c - '0';
                last = c - '0';
            } else {
                for (numwords) |nw| {
                    if ((line.len - ix) < nw.w.len)
                        continue;
                    const pw = line[ix..(ix + nw.w.len)];
                    if (std.mem.eql(u8, nw.w, pw)) {
                        if (first == null)
                            first = nw.n;
                        last = nw.n;
                    }
                }
            }
        }

        const num: u64 = (first orelse @panic("WHA")) * 10 + last;

        try w.print("{s:30} -> {d:2}\n", .{line, num});
        sum += num;
    }

    try w.print("sum = {}\n", .{sum});
    try bw.flush();
}
