const std = @import("std");

pub fn main() !void
{
    std.debug.print("give input:\n", .{});

    var out = std.io.getStdOut();
    var bw = std.io.bufferedWriter(out.writer());
    var w = bw.writer();

    var sum: u64 = 0;
    var in = std.io.getStdIn();
    var br = std.io.bufferedReader(in.reader());
    var rd = br.reader();
    var buf: [1024]u8 = undefined;
    var fbs = std.io.fixedBufferStream(&buf);
    var fbw = fbs.writer();
    while (true) {
        fbs.reset();
        rd.streamUntilDelimiter(fbw, '\n', buf.len) catch |err| {
            if (err == error.EndOfStream) {
                break;
            } else {
                return err;
            }
        };

        const line = fbs.getWritten();
        if (line.len == 0)
            break;

        var first: ?u8 = null;
        var last: u8 = 0;
        for (line) |c| {
            if (c >= '0' and c <= '9') {
                if (first == null)
                    first = c - '0';
                last = c - '0';
            }
        }

        const num: u64 = (first orelse @panic("WHA")) * 10 + last;

        try w.print("{s:30} -> {d:2}\n", .{line, num});
        sum += num;
    }

    try w.print("sum = {}\n", .{sum});
    try bw.flush();
}
