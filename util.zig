const std = @import("std");

pub const parse = struct {
    pub const Error = error {
        Syntax,
        Mismatch,
        None,
    };

    pub const CharCheckFn = *const fn (char: u8) bool;

    pub fn readUInt(rl: *[]const u8, comptime T: type) Error!T
    {
        const ti = @typeInfo(T);
        std.debug.assert(ti == .Int);
        std.debug.assert(ti.Int.signedness == .unsigned);
        if (rl.*.len <= 0)
            return Error.None;
        if (rl.*[0] < '0' or rl.*[0] > '9')
            return Error.None;

        var result: T = rl.*[0] - '0';
        rl.* = rl.*[1..];

        while (rl.*[0] >= '0' and rl.*[0] <= '9') {
            result *= 10;
            result += rl.*[0] - '0';
            rl.* = rl.*[1..];
        }

        return result;
    }

    pub fn readWord(rl: *[]const u8, charcheck: CharCheckFn)
        Error![]const u8
    {
        while (rl.*.len > 0 and rl.*[0] == ' ')
            rl.* = rl.*[1..];
        if (rl.*.len == 0)
            return Error.Syntax;

        var ix: usize = 0;
        while (ix < rl.*.len and charcheck(rl.*[ix]))
            ix += 1;

        if (ix == 0)
            return Error.None;

        const ret = rl.*[0..ix];
        rl.* = rl.*[ix..];
        return ret;
    }

    pub fn maybeToken(rl: *[]const u8, token: []const u8) bool
    {
        if (rl.*.len < token.len)
            return false;
        if (!std.mem.eql(u8, rl.*[0..token.len], token))
            return false;
        rl.* = rl.*[token.len..];
        return true;
    }

    pub fn expectToken(rl: *[]const u8, token: []const u8) Error!void
    {
        if (!maybeToken(rl, token))
            return Error.Syntax;
    }

    pub fn expectSpace(rl: *[]const u8) Error!void
    {
        if (rl.*.len < 1 or rl.*[0] != ' ')
            return Error.Syntax;
        while (true) {
            rl.* = rl.*[1..];
            if (rl.*.len < 1 or rl.*[0] != ' ')
                return;
        }
    }

    pub fn skipSpace(rl: *[]const u8) void
    {
        while (rl.*.len > 0 and rl.*[0] == ' ')
            rl.* = rl.*[1..];
    }

    pub fn expectWord(rl: *[]const u8,
                      word: []const u8,
                      charcheck: CharCheckFn) Error!void
    {
        const rl_word = try readWord(rl, charcheck);
        if (!std.mem.eql(u8, rl_word, word))
            return Error.Mismatch;
    }

    pub fn isAlpha(char: u8) bool
    {
        return (char >= 'a' and char <= 'z') or (char >= 'A' and char <= 'Z');
    }
};

