const std = @import("std");
const util = @import("util.zig");

const GAME_TOKEN = "Game";
const Color = enum {
    red,
    green,
    blue,

    fn ofName(name: []const u8) !Color
    {
        inline for (std.meta.fields(Color)) |field| {
            if (std.mem.eql(u8, name, field.name))
                return @enumFromInt(field.value);
        }

        return error.InvalidName;
    }
};

const Drawn = struct {
    reds: u16 = 0,
    greens: u16 = 0,
    blues: u16 = 0,

    fn maxOfGroup(group: []const Drawn) Drawn
    {
        var result = Drawn{};
        for (group) |drawn| {
            result.reds = @max(result.reds, drawn.reds);
            result.greens = @max(result.greens, drawn.greens);
            result.blues = @max(result.blues, drawn.blues);
        }
        return result;
    }

    fn power(self: *const Drawn) u64
    {
        return self.reds * self.greens * self.blues;
    }

    fn addColor(self: *Drawn, color: Color, value: u16) void
    {
        switch (color) {
            .red   => self.reds += value,
            .green => self.greens += value,
            .blue  => self.blues += value,
        }
    }
};

const Game = struct {
    id: u64,
    drawn: []const Drawn,
    allocator: std.mem.Allocator,

    fn deinit(self: *const Game) void
    {
        self.allocator.free(self.drawn);
    }

    fn parseLine(line: []const u8, alloc: std.mem.Allocator) !Game
    {
        var list = std.ArrayList(Drawn).init(alloc);
        errdefer list.deinit();

        var rl = line;
        try util.parse.expectWord(&rl, GAME_TOKEN, util.parse.isAlpha);
        util.parse.skipSpace(&rl);
        const id = try util.parse.readUInt(&rl, u64);
        try util.parse.expectToken(&rl, ":");

        while (rl.len > 0) {
            var drawn = Drawn{};

            while (rl.len > 0) {
                util.parse.skipSpace(&rl);
                const num = util.parse.readUInt(&rl, u16) catch |e| {
                    if (e != util.parse.Error.None or rl.len > 0)
                        return e;
                    continue;
                };
                try util.parse.expectSpace(&rl);
                const color_word = try util.parse.readWord(&rl, util.parse.isAlpha);
                drawn.addColor(try Color.ofName(color_word), num);

                if (util.parse.maybeToken(&rl, ","))
                    continue;
                if (util.parse.maybeToken(&rl, ";"))
                    break;
            }

            try list.append(drawn);
        }

        const result_drawn = try list.toOwnedSlice();
        return .{
            .id = id,
            .drawn = result_drawn,
            .allocator = alloc,
        };
    }
};


pub fn main() !void
{
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};

    const out = std.io.getStdOut();
    var out_bw = std.io.bufferedWriter(out.writer());
    const out_wr = out_bw.writer();

    const in = std.io.getStdIn();
    var in_br = std.io.bufferedReader(in.reader());
    const in_rd = in_br.reader();

    var buf: [1024]u8 = undefined;
    var buf_st = std.io.fixedBufferStream(&buf);
    const buf_wr = buf_st.writer();

    var sum: u64 = 0;
    while (true) {
        buf_st.reset();
        in_rd.streamUntilDelimiter(buf_wr, '\n', buf.len) catch |err| {
            if (err == error.EndOfStream) {
                break;
            } else {
                return err;
            }
        };

        const line = buf_st.getWritten();
        if (line.len == 0)
            break;

        const game = try Game.parseLine(line, gpa.allocator());
        defer game.deinit();
        sum += Drawn.maxOfGroup(game.drawn).power();
    }

    try out_wr.print("Sum of powers: {}\n", .{sum});
    try out_bw.flush();
}
